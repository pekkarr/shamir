# Shamir's secret sharing

A Java implementation of Shamir's secret sharing. This program splits a file into multiple files, which are needed to reconstruct the original file.

## Implementation

The used finite field is GF(2^31-1).

## License

Licensed under GPLv3+.
