package org.pekkarr.shamir;

import static org.pekkarr.shamir.Util.P;

public class Combiner {
	private final int[] xvalues;
	private final int k;

	public Combiner(int[] xvalues) {
		this.xvalues = xvalues;
		this.k = xvalues.length;
	}

	public int combine(int[] shares) {
		if (shares.length != k)
			throw new IllegalArgumentException("Wrong number of shares");

		long sum = 0;
		for (int j = 0; j < k; j++) {
			int xj = xvalues[j];
			long product = 1;
			for (int m = 0; m < k; m++) {
				if (m == j) continue;
				int xm = xvalues[m];
				product = product * xm % P;
				int diff = xm - xj;
				if (diff < 0) diff += P;
				product = product * modularInverse(diff) % P;
			}
			sum = (sum + shares[j] * product % P) % P;
		}
		return (int) sum;
	}

	private static int modularInverse(int a) {
		int r = P, newr = a, t = 0, newt = 1;
		while (newr != 0) {
			int tmp = r;
			r = newr;
			int q = tmp / newr;
			newr = tmp % newr;

			tmp = t;
			t = newt;
			newt = tmp - q * newt;
		}
		if (t < 0) t += P;
		return t;
	}
}
