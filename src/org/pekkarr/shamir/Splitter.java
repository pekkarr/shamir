package org.pekkarr.shamir;

import static org.pekkarr.shamir.Util.P;
import static org.pekkarr.shamir.Util.nextInt;

public class Splitter {
	private int evaluate(int x) {
		long value = poly[k - 1];
		for (int i = k - 2; i >= 0; i--) {
			value = value * x % P;
			value = (value + poly[i]) % P;
		}
		return (int) value;
	}

	private final int k, n;
	private final int[] xvalues, poly;

	public Splitter(int k, int[] xvalues) {
		this.k = k;
		this.n = xvalues.length;
		if (0 >= k || k > n || n >= P)
			throw new IllegalArgumentException("Invalid split parameters");
		this.xvalues = xvalues;
		this.poly = new int[k];
	}

	public void split(int secret, int[] shares) {
		if (secret >= P || shares.length != n)
			throw new IllegalArgumentException("Invalid split parameters");

		poly[0] = secret;
		for (int i = 1; i < k; i++)
			poly[i] = nextInt();

		for (int i = 0; i < n; i++)
			shares[i] = evaluate(xvalues[i]);
//		while (shares.size() < n) {
//			int x;
//			do x = rnd.nextInt(P); while (shares.containsKey(x));
//			shares.put(x, evaluate(poly, x));
//		}
	}

	public static void main(String[] args) {
		int secret = 123456789, k = 1000, n = k;
		int[] xvalues = new int[n];
		for (int i = 0; i < n; i++) xvalues[i] = i + 1;
		Splitter splitter = new Splitter(k, xvalues);
		int[] shares = new int[k];
		splitter.split(secret, shares);
		for (int i = 0; i < n; i++)
			System.out.println("(" + xvalues[i] + ", " + shares[i] + ")");
		Combiner c = new Combiner(xvalues);
		System.out.println("Result: " + c.combine(shares));
//		LinkedHashMap<Integer, Integer> shares = split(secret, k, n);
//		shares.entrySet().stream()
//				.map(e -> "(" + e.getKey() + ", " + e.getValue() + ")")
//				.forEach(System.out::println);
//		int result = combine(shares.entrySet().stream()
//				.limit(k)
//				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
//		System.out.println("Result: " + result);
	}



//	@SuppressWarnings("unused")
//	private static class Old {
//		public static class Shares {
//			private ArrayList<BigInteger> shares;
//			private BigInteger P;
//			private int k, n;
//		}
//
//		public class Share {
//			private final int x;
//			private final BigInteger fx;
//
//			public Share(int x, BigInteger fx) {
//				this.x = x;
//				this.fx = fx;
//			}
//		}
//
//		public static Shares split(BigInteger s, int k, int n) {
//			if (k > n)
//				throw new IllegalArgumentException("k > n, secret wouldn't be recoverable");
//			Shares result = new Shares();
//			result.shares = new ArrayList<>(n);
//			do {
//				result.P = new BigInteger(s.bitLength() + 1, 200, rnd);
//			} while (s.compareTo(result.P) >= 0);
//			result.k = k;
//			result.n = n;
//			final int bitLength = result.P.bitLength();
//
//			ArrayList<BigInteger> poly = new ArrayList<>(k);
//			poly.set(0, s);
//			for (int i = 1; i < k; i++) {
//				BigInteger r;
//				do {
//					r = new BigInteger(bitLength, rnd);
//				} while (r.compareTo(result.P) >= 0);
//				poly.set(i, r);
//			}
//
//			for (int i = 1; i <= n; i++) {
//				BigInteger val = poly.get(k - 1), bigI = valueOf(i);
//				for (int j = k - 2; j >= 0; j--)
//					val = val.multiply(bigI).mod(result.P).add(poly.get(j)).mod(result.P);
//				result.shares.set(i - 1, val);
//			}
//
//			return result;
//		}
//
//		public static BigInteger combine(ArrayList<Share> shares, BigInteger P) {
//			final int k = shares.size();
//			BigInteger result = ZERO;
//			for (int j = 0; j < k; j++) {
//				Share sj = shares.get(j);
//				BigInteger xj = valueOf(sj.x), product = ONE;
//				for (int m = 0; m < k; m++) {
//					if (m == j)
//						continue;
//					BigInteger xm = valueOf(shares.get(m).x);
//					product = product.multiply(xm).mod(P);
//					product = product.multiply(xm.subtract(xj).modInverse(P)).mod(P);
//				}
//				result = result.add(sj.fx.multiply(product).mod(P)).mod(P);
//			}
//			return result;
//		}
//	}
}
