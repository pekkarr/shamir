package org.pekkarr.shamir;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;

public class Util {
	public static final int P = (1 << 31) - 1;
	public static final SecureRandom RNG = new SecureRandom();
	public static int nextInt() {
		return RNG.nextInt(P);
	}

	static List<SeekableByteChannel> openChannels(List<Path> paths, OpenOption... options) throws IOException {
		try {
			return paths.stream()
					.map(p -> {
						try {
							return Files.newByteChannel(p, options);
						} catch (IOException e) {
							throw new UncheckedIOException(e);
						}
					})
					.collect(Collectors.toList());
		} catch (UncheckedIOException e) {
			throw e.getCause();
		}
	}

	public static final class FILE_SPECS {
		public static final long MAGIC = 0x2741_5a1a_7839_5723L;
		public static final short FILE_FORMAT_VERSION = 1;
		public static final String FILENAME_EXTENSION = ".sss";
	}

	static void eraseBuf(ByteBuffer buf) {
		buf.clear();
		while (buf.remaining() >= Long.BYTES)
			buf.putLong(0L);
		while (buf.hasRemaining())
			buf.put((byte) 0);
		buf.clear();
	}
}
