package org.pekkarr.shamir;

import static org.pekkarr.shamir.Util.P;
import static org.pekkarr.shamir.Util.RNG;
import static org.pekkarr.shamir.Util.FILE_SPECS.FILENAME_EXTENSION;
import static org.pekkarr.shamir.Util.FILE_SPECS.FILE_FORMAT_VERSION;
import static org.pekkarr.shamir.Util.FILE_SPECS.MAGIC;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileSplitter {
	private static final int BUF_SIZE = 8192;

	private final int n, k;
	private final ByteBuffer inputBuf = ByteBuffer.allocateDirect(BUF_SIZE);
	private final ByteBuffer[] outputBufs;
	private final int[] secret = new int[4];
	private final int[][] shares;
	private final int[] xvalues;
	private final Splitter splitter;

	public FileSplitter(int n, int k) {
		this(k, generateXValues(n));
	}

	public FileSplitter(int k, int[] xvalues) {
		this.n = xvalues.length;
		this.k = k;
		outputBufs = new ByteBuffer[n];
		Arrays.setAll(outputBufs, i -> ByteBuffer.allocateDirect(BUF_SIZE));
		shares = new int[secret.length][n];
		this.xvalues = xvalues;
		splitter = new Splitter(k, xvalues);
	}

	/** Generate {@code n} distinct values for x. */
	private static int[] generateXValues(int n) {
		int[] xvalues = new int[n];
		LinkedHashSet<Integer> xSet = new LinkedHashSet<>(n * 4 / 3 + 1);
		while (xSet.size() < n) {
			int x;
			do x = RNG.nextInt(P - 1) + 1; while (xSet.contains(x));
			xvalues[xSet.size()] = x;
			xSet.add(x);
		}
		return xvalues;
	}

	private void outputXValues() throws IOException {
		for (int i = 0; i < n; i++)
			outputBufs[i].putInt(xvalues[i]);
	}

	private void flushBufs(List<? extends WritableByteChannel> out) throws IOException {
		Iterator<? extends WritableByteChannel> iter = out.iterator();
		for (int i = 0; i < outputBufs.length; i++) {
			WritableByteChannel ch = iter.next();
			ByteBuffer buf = outputBufs[i];
			buf.flip();
			do ch.write(buf); while (buf.hasRemaining());
			buf.clear();
		}
	}

	public void split(Path in, String prefix, String postfix) throws IOException {
		List<Path> out = Arrays.stream(xvalues)
				.mapToObj(x -> String.format("%08x", x))
				.map(x -> prefix + x + postfix + FILENAME_EXTENSION)
				.map(Paths::get)
				.collect(Collectors.toList());
		split(in, out);
	}

	public void split(Path in, List<Path> out) throws IOException {
		List<SeekableByteChannel> outChannels = Util.openChannels(out, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		SeekableByteChannel inChannel = Files.newByteChannel(in, StandardOpenOption.READ);
		split(inChannel, outChannels);
		inChannel.close();
		for (Iterator<SeekableByteChannel> iter = outChannels.iterator(); iter.hasNext();)
			iter.next().close();
	}

	private static final int byteParsingSize = 15;

	public void split(ReadableByteChannel ich, List<? extends WritableByteChannel> out) throws IOException {
		if (out.size() != n)
			throw new IllegalArgumentException("out.size() != n");

		outputHeader();
		outputXValues();

		while (ich.read(inputBuf) != -1) {
			inputBuf.flip();
			while (inputBuf.remaining() >= byteParsingSize) {
				splitInToOut(out);
			}
			inputBuf.compact();
		}
		inputBuf.flip();
		int remaining = inputBuf.remaining();
		if (remaining > 0) {
			inputBuf.limit(byteParsingSize);
			splitInToOut(out);
		}
		flushIfNecessary(out);
		int endInt = Util.P + byteParsingSize - remaining;
		for (int i = 0; i < n; i++)
			outputBufs[i].putInt(endInt);

		flushBufs(out);

		Stream.concat(Arrays.stream(shares), Stream.of(secret, xvalues))
				.forEach(arr -> Arrays.fill(arr, 0));
		Stream.concat(Arrays.stream(outputBufs), Stream.of(inputBuf))
				.forEach(Util::eraseBuf);
	}

	private void outputHeader() {
		long split_id = Util.RNG.nextLong();
		for (int i = 0; i < n; i++) {
			ByteBuffer buf = outputBufs[i];
			buf.putLong(MAGIC);
			buf.putShort(FILE_FORMAT_VERSION);
			buf.putLong(split_id);
			buf.putInt(k);
		}
	}

	private void splitInToOut(List<? extends WritableByteChannel> out) throws IOException {
		parseInput(inputBuf, secret);

		for (int i = 0; i < secret.length; i++) {
			splitter.split(secret[i], shares[i]);
			flushIfNecessary(out);
			for (int j = 0; j < n; j++)
				outputBufs[j].putInt(shares[i][j]);
		}
	}

	private void flushIfNecessary(List<? extends WritableByteChannel> out) throws IOException {
		if (outputBufs[0].remaining() < Integer.BYTES)
			flushBufs(out);
	}

	/** Copy data from {@code buf} to {@code secret}. */
	private static void parseInput(ByteBuffer buf, int[] secret) {
		byte tmp;
		secret[0] = (buf.get() << 22)			| (buf.get() << 14)	| (buf.get() << 6)	| ((tmp = buf.get()) >> 2);
		secret[1] = ((tmp & 0b00_00_11) << 28)	| (buf.get() << 20)	| (buf.get() << 12)	| (buf.get() << 4)	| ((tmp = buf.get()) >> 4);
		secret[2] = ((tmp & 0b00_11_11) << 26)	| (buf.get() << 18)	| (buf.get() << 10)	| (buf.get() << 2)	| ((tmp = buf.get()) >> 6);
		secret[3] = ((tmp & 0b11_11_11) << 24)	| (buf.get() << 16)	| (buf.get() << 8)	| buf.get();
	}

	public static void main(String[] args) {
		FileSplitter splitter = new FileSplitter(4, 4);
		try {
			splitter.split(Paths.get(args[0]), "test-", "");
		} catch (IOException e) {
			System.err.println("IO error occurred");
			e.printStackTrace();
		}
	}
}
