package org.pekkarr.shamir;

import static org.pekkarr.shamir.Util.FILE_SPECS.FILE_FORMAT_VERSION;
import static org.pekkarr.shamir.Util.FILE_SPECS.MAGIC;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileCombiner {
	private static final int BUF_SIZE = 8192;

	private final ByteBuffer[] inputBufs;
	private final ByteBuffer outputBuf = ByteBuffer.allocateDirect(BUF_SIZE);
	private final Combiner combiner;
	private final int k;

	private final List<? extends ReadableByteChannel> inList;
	private final WritableByteChannel out;

	public FileCombiner(List<Path> inList, Path out) throws IOException {
		this(Util.openChannels(inList, StandardOpenOption.READ), Files.newByteChannel(out, StandardOpenOption.WRITE, StandardOpenOption.CREATE));
	}

	public FileCombiner(List<? extends ReadableByteChannel> inList, WritableByteChannel out) throws IOException {
		this.inList = inList;
		this.out = out;
		int inSize = inList.size();
		inputBufs = new ByteBuffer[inSize];
		Iterator<? extends ReadableByteChannel> inIter = inList.iterator();
		long split_id = 0;
		int k = 0;
		int[] xvalues = new int[inSize];
		HashSet<Integer> xSet = new HashSet<>(inSize * 4 / 3);
		for (int i = 0; i < inSize; i++) {
			ByteBuffer inputBuf = inputBufs[i] = ByteBuffer.allocateDirect(BUF_SIZE);
			readHeaderAndX(inIter.next(), inputBuf);
			inputBuf.flip();
			if (inputBuf.getLong() != MAGIC)
				throw new IllegalArgumentException("Wrong magic number");
			short file_version = inputBuf.getShort();
			if (file_version > FILE_FORMAT_VERSION)
				throw new IllegalArgumentException("File type version " + file_version + " not supported");
			if (i == 0) {
				split_id = inputBuf.getLong();
				k = inputBuf.getInt();
				if (k != inSize)
					throw new IllegalArgumentException("Wrong number of share files, " + k + " shares are needed");
			} else {
				if (inputBuf.getLong() != split_id)
					throw new IllegalArgumentException("Some inputs belong to different splits");
				if (inputBuf.getInt() != k) // This should not happen, when split_ids are equal
					throw new IllegalArgumentException("Malformed share file");
			}
			int x = xvalues[i] = inputBuf.getInt();
			if (!xSet.add(x))
				throw new IllegalArgumentException("Same x value " + x + " multiple times");
		}
		this.combiner = new Combiner(xvalues);
		this.k = k;
	}

	private static final int headerAndXBytes = 26;
	private static void readHeaderAndX(ReadableByteChannel in, ByteBuffer buf) throws IOException {
		if (readBytes(in, buf, headerAndXBytes) < headerAndXBytes)
			throw new IllegalArgumentException("Too short input stream");
	}

	/**
	 * Read at least {@code n} bytes from Channel {@code in} to ByteBuffer {@code buf} if possible.
	 * If the Channel reaches end-of-stream, only the remaining bytes are read.
	 * @return number of bytes read
	 */
	private static int readBytes(ReadableByteChannel in, ByteBuffer buf, final int n) throws IOException {
		int totalBytesRead = 0, bytesRead;
		while (totalBytesRead < n && (bytesRead = in.read(buf)) != -1)
			totalBytesRead += bytesRead;
		return totalBytesRead;
	}

	private static final int INTEGERS_PER_READ = 4;
	private static final int INPUT_SIZE = INTEGERS_PER_READ * Integer.BYTES;

	public void combine() throws IOException {
		int[][] input = new int[INTEGERS_PER_READ][k];
		int[] secret = new int[INTEGERS_PER_READ];
		boolean last = false;
		int bytesToSkip = 0;
		while (!last) {
			Iterator<? extends ReadableByteChannel> inIter = inList.iterator();
			for (int i = 0; i < k; i++) {
				ReadableByteChannel in = inIter.next();
				ByteBuffer inputBuf = inputBufs[i];
				if (inputBuf.remaining() < INPUT_SIZE) {
					inputBuf.compact();
					readBytes(in, inputBufs[i], INPUT_SIZE - inputBuf.position());
					inputBuf.flip();
					if (inputBuf.remaining() < INPUT_SIZE) {
						if (!last) {
							if (i != 0)
								throw new IllegalArgumentException("Malformed input file"); // TODO create own exceptions
							last = true;
							bytesToSkip = inputBuf.getInt() - Util.P;
						} else if (bytesToSkip != inputBuf.getInt() - Util.P)
							throw new IllegalArgumentException("Malformed input file");
					}
				}
				if (!last) for (int j = 0; j < input.length; j++)
					input[j][i] = inputBuf.getInt();
			}
			if (!last) {
				for (int j = 0; j < input.length; j++)
					secret[j] = combiner.combine(input[j]);
				generateOutput(secret);
			}
		}
		outputBuf.flip();
		outputBuf.limit(outputBuf.limit() - bytesToSkip);
		do out.write(outputBuf); while (outputBuf.hasRemaining());

		Stream.concat(Arrays.stream(input), Stream.of(secret))
				.forEach(ar -> Arrays.fill(ar, 0));
		Stream.concat(Arrays.stream(inputBufs), Stream.of(outputBuf))
				.forEach(Util::eraseBuf);
	}

	private void generateOutput(int[] secret) throws IOException {
		while (outputBuf.remaining() < 15) { // TODO replace 15 by constant
			outputBuf.flip();
			out.write(outputBuf);
			outputBuf.compact();
		}
		outputBuf.put((byte) (secret[0] >> 22))
				.put((byte) (secret[0] >> 14))
				.put((byte) (secret[0] >> 6))
				.put((byte) ((secret[0] << 2) | (secret[1] >> 28)))
				.put((byte) (secret[1] >> 20))
				.put((byte) (secret[1] >> 12))
				.put((byte) (secret[1] >> 4))
				.put((byte) ((secret[1] << 4) | (secret[2] >> 26)))
				.put((byte) (secret[2] >> 18))
				.put((byte) (secret[2] >> 10))
				.put((byte) (secret[2] >> 2))
				.put((byte) ((secret[2] << 6) | (secret[3] >> 24)))
				.put((byte) (secret[3] >> 16))
				.put((byte) (secret[3] >> 8))
				.put((byte) secret[3]);
	}

	public static void main(String[] args) throws IOException {
		FileCombiner fc = new FileCombiner(Arrays.stream(args)
				.map(Paths::get)
				.collect(Collectors.toList()), Paths.get("output.txt"));
		fc.combine();
	}
}
